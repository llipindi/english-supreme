<?php

namespace App\Http\Controllers;

use App\Helpers\Globals;
use App\Models\Apps;
use App\Models\Channel;
use App\Models\DownloadUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Exception\ProcessSignaledException;
use Symfony\Component\Process\Process;

class MainController extends Controller
{
    public function home()
    {
        return view('home');
    }
}
