@extends('layout.app')

@section('content')
    <!-- Header Section Start -->
    {{--<header id="slider-area">--}}
    <nav class="navbar navbar-expand-md fixed-top scrolling-navbar bg-white">
        <div class="container">
            <a class="navbar-brand" href="{{route('home')}}"
               style="color: #000;background-color: #feff04;padding-right: 10px;">
                <img src="{{ asset('img/logo4.png') }}"/> ENGLISH SUPREME
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                    aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <i class="lni-menu"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mr-auto w-100 justify-content-end">
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#slider-area">Beranda</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#about">English Supreme</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#services">Program dan Jasa Kami</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#contact">Hubungi Kami</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    {{--<div id="carousel-area">--}}
    {{--<div id="carousel-slider" class="carousel slide carousel-fade" data-ride="carousel">--}}
    {{--<ol class="carousel-indicators">--}}
    {{--<li data-target="#carousel-slider" data-slide-to="0" class="active"></li>--}}
    {{--<li data-target="#carousel-slider" data-slide-to="1"></li>--}}
    {{--</ol>--}}
    {{--<div class="carousel-inner" role="listbox">--}}
    {{--<div class="carousel-item active">--}}

    {{--<div class="carousel-caption text-left">--}}
    {{--<h3 class="wow fadeInRight" data-wow-delay="0.2s">Handcrafted</h3>--}}
    {{--<h2 class="wow fadeInRight" data-wow-delay="0.4s">English Supreme</h2>--}}
    {{--<h4 class="wow fadeInRight" data-wow-delay="0.6s">Comes with All Essential Sections &--}}
    {{--Elements</h4>--}}
    {{--<a href="#" class="btn btn-lg btn-common btn-effect wow fadeInRight" data-wow-delay="0.9s">Download</a>--}}
    {{--<a href="#" class="btn btn-lg btn-border wow fadeInRight" data-wow-delay="1.2s">Get Started!</a>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="carousel-item">--}}
    {{--<img src="{{asset('img/slider/slider1.jpg')}}" alt="" style="width:100%;margin: auto;">--}}
    {{--<div class="carousel-caption text-center">--}}
    {{--<h3 class="wow fadeInDown" data-wow-delay="0.3s">Bundled With Tons of</h3>--}}
    {{--<h2 class="wow bounceIn" data-wow-delay="0.6s">Yes You Can!</h2>--}}
    {{--<h4 class="wow fadeInUp" data-wow-delay="0.9s">Parallax, Video, Product, Premium Addons and--}}
    {{--More...</h4>--}}
    {{--<a href="#" class="btn btn-lg btn-common btn-effect wow fadeInUp" data-wow-delay="1.2s">View Works</a>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<a class="carousel-control-prev" href="#carousel-slider" role="button" data-slide="prev">--}}
    {{--<span class="carousel-control" aria-hidden="true"><i class="lni-chevron-left"></i></span>--}}
    {{--<span class="sr-only">Previous</span>--}}
    {{--</a>--}}
    {{--<a class="carousel-control-next" href="#carousel-slider" role="button" data-slide="next">--}}
    {{--<span class="carousel-control" aria-hidden="true"><i class="lni-chevron-right"></i></span>--}}
    {{--<span class="sr-only">Next</span>--}}
    {{--</a>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</header>--}}
    <!-- Header Section End -->
    <div id="carouselExampleIndicators" class="container carousel slide" data-ride="carousel"
         style="margin-top: 100px;">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="{{asset('img/slide4.jpg')}}" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{asset('img/slide1.jpg')}}" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{asset('img/slide2.jpg')}}" alt="Second slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{asset('img/slide3.jpg')}}" alt="Third slide">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    {{--<section class="section" style="background-color: #fff;margin-top: 40px;padding-bottom: 0 !important;">--}}
    {{--<img src="{{asset('img/slider/slider3.jpg')}}" alt="" style="width:100%;margin: auto;">--}}
    {{--</section>--}}
    <!-- Services Section Start -->
    <section id="about" class="section" style="background-color: #fff;padding-top: 40px !important;">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title">Tentang Kami</h2>
                <span>English Supreme</span>
                <p class="section-subtitle">
                    Fokus usaha dan karya English Supreme adalah pada bidang jasa pengajaran Bahasa Inggris. English
                    Supreme memiliki dua sistem pengajaran luring (offline) dan daring (online) . Sistem luring
                    (offline) memfokuskan pada proses belajar In-Home Tutoring dan In-Company Training. Sedangkan daring
                    (online) dilakukan dengan metode jarak jauh dengan menggunakan aplikasi komunikasi melalui jaringan
                    internet. Siswa dapat memilih untuk menggunakan metode yang sesuai dengan kebutuhan dan minatnya.
                </p>
            </div>
            <div class="row text-justify" style="padding: 20px;">
                <h5 style="font-size: 20px; margin: 20px 0;">Apa itu Home Tutoring?</h5>
                <p>
                    <i>In-Home Tutoring</i> adalah konsep belajar dimana seorang tutor (pengajar) membimbing 1 siswa
                    ataupun
                    sekelompok kecil siswa dimana proses belajar dan mengajar di lakukan di rumah atau tempat yang
                    bersifat kondusif yang ditentukan oleh siswa sendiri. Dengan kata lain kelas hanya dilakukan dengan
                    sistem privat dan semi privat.
                </p>
                <h5 style="font-size: 20px; margin: 20px 0;">Kenapa harus In Home Tutoring?</h5>
                <p>
                    Banyak orang merasa tidak nyaman dan tidak fokus untuk belajar bersama dengan kelompok lain yang
                    mereka tidak kenal terutama jika kelas diisi oleh lebih dari 5 orang karena selain murid yang tidak
                    fokus belajar, guru atau pengajar juga tidak bisa memperhatikan siswa secara maksimal. Perasaan
                    sungkan dan malu untuk berbicara dalam bahasa Inggris di depan banyak orang juga merupakan alasan
                    untuk belajar dengan sistem ini. Sistem belajar privat atau in-home tutoring memudahkan siswa untuk
                    percaya diri belajar dan menerapkan bahasa Inggris dengan baik dan benar tanpa malu ditertawakan
                    oleh teman-teman sekelas yang tidak mereka kenal. Orang tua siswa juga tidak perlu khawatir dengan
                    kondisi anaknya karena siswa belajar di rumah sehingga tetap dalam pengawasan orang tua. Konsep
                    belajar in-home tutoring yang adalah fokus dari bisnis kami ini dengan menyasar pada banyaknya
                    permintaaan dari pembelajar untuk bisa belajar dengan lebih terarah dan fokus pada materi yang
                    diajarkan. Beberapa siswa kami juga sangat memperhatikan privasi sehingga konsep ini cocok dengan
                    mereka. Di lain pihak, fleksibilitas waktu dan tempat juga menjadi alasan memilih konsep belajar
                    in-home tutoring.
                </p>
                <p>
                    Selain In-Home Tutoring, kami juga memiliki sistem belajar In-Company Training dengan program yang
                    ditujukan untuk para profesional dan perusahaan yang berniat meningkatkan kinerja sumber daya
                    manusia yang mereka miliki seiring dengan meningkatnya kemampuan bahasa Inggris mereka. Program
                    untuk In-Company Training disesuaikan dengan kebutuhan instansi, organisasi ataupun perusahaan.
                </p>
                <p style="margin-top: 20px !important;"><img src="{{asset('img/info1.png')}}" style="max-width: 100%;"
                                                             alt="info"/></p>
                <p style="margin-bottom: 20px !important;">Sumber :
                    https://www.cambridgeenglish.org/in/exams-and-tests/cefr/</p>
                <p>
                    Setiap siswa wajib melakukan Placement Test untuk program yang diambil sebagai standar prosedur dari
                    kami. Setiap siswa akan dibimbing sesuai dengan level dan target level yang diharapkan oleh siswa.
                    Proses belajar mengajar yang kami berikan dilakukan secara profesional dengan melihat kemampuan
                    siswa atau pembelajar yang disesuaikan dengan kurikulum yang kami miliki. Standar materi yang
                    diberikan kepada siswa disesuaikan dengan level CEFR (Common European Framework of Reference). CEFR
                    adalah standar untuk mengukur tingkat kemampuan bahasa seseorang. Standar CEFR sudah digunakan
                    secara internasional yang tidak hanya digunakan untuk mengukur tingkat kemampuan bahasa Inggris,
                    namun juga digunakan untuk mengukur kemampuan bahasa lain di dunia Kami menerapkan standar level
                    CEFR untuk penempatan level dari program yang diambil oleh siswa.
                </p>
                <h5 style="font-size: 20px; margin: 20px 0;">Pengajaran Sistem Daring (Online Learning)</h5>
                <p>
                    Dengan meningkatnya persaingan global di seluruh dunia maka masyarakat dunia termasuk Indonesia
                    dituntut untuk bisa menguasai bahasa Inggris. Seiring dengan kemajuan teknologi dan komunikasi
                    internet maka pembelajaran pun berkembang dengan sistem daring (online). Kelebihan sistem ini adalah
                    siswa dapat belajar dimana saja secara privat dan memungkinkan siswa untuk tetap belajar meskipun
                    kondisi tidak memungkinkan seperti yang terjadi di masa pandemi covid 19.
                </p>
            </div>
        </div>
    </section>
    <!-- Services Section End -->

    <div class="counters section bg-defult" style="padding: 50px 0 !important;">
        <div class="container">
        </div>
    </div>

    <!-- Services Section Start -->
    <section id="services" class="section">
        <div class="container">
            <div class="section-header" style="margin-bottom: 60px;">
                <h2 class="section-title">Program dan Jasa Kami</h2>
                <span>Offline and Online Class</span>
            </div>
            <div class="row">
                <div class="text-justify" style="padding:30px;">
                    <h5 style="font-size: 20px; margin: 20px 0;">Program Kelas Luring (Offline Class)</h5>
                    <p>
                        Di kelas luring (offline) kami memiliki program IELTS, Conversation Class (General English),
                        Business English dan English for kids.
                    </p>
                </div>
                <div class="row" style="padding:30px;">
                    <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="item-boxes services-item wow fadeInDown" data-wow-delay="0.2s">
                            <img src="{{asset('img/programs/ielts.jpg')}}"
                                 style="width: 100%;margin:0 !important;"/>
                            <div class="p-3" style="min-height: 315px;">
                                <h3>IELTS Programs</h3>
                                <p>
                                    IELTS digunakan di banyak negara untuk keperluan
                                    sekolah, migrasi, dan bekerja di negara-negara yang menggunakan bahasa Inggris
                                    sebagai
                                    bahasa pertamanya seperti Amerika, Inggris, Australia dan bahkan negara-negara Eropa
                                    dan
                                    Asia. ENGLISH SUPREME memiliki program belajar persiapan tes IELTS
                                    untuk
                                    kalian yang membutuhkan. Hubungi kami untuk info lebih lanjut
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="item-boxes services-item wow fadeInDown" data-wow-delay="0.2s">
                            <img src="{{asset('img/programs/conv.jpg')}}"
                                 style="width: 100%;margin:0 !important;"/>
                            <div class="p-3" style="min-height: 315px;">
                                <h3>Conversation Class</h3>
                                <p>
                                    Ingin berbicara bahasa Inggris dengan lancar? Kami memiliki program Conversation
                                    private
                                    class sesuai dengan konsep in home tutoring. Kamu tidak perlu malu dan sungkan lagi
                                    berbicara dengan vocabulary (kosakata) dan grammar (tata bahasa) yang berantakan
                                    karena
                                    private tutor akan membimbing kamu dengan sabar dan terarah agar kamu bisa
                                    menggunakan
                                    bahasa Inggris dengan baik. Silakan hubungi kami
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="item-boxes services-item wow fadeInDown" data-wow-delay="0.2s">
                            <img src="{{asset('img/programs/be.jpg')}}"
                                 style="width: 100%;margin:0 !important;"/>
                            <div class="p-3" style="min-height: 315px;">
                                <h3>Bussines English</h3>
                                <p>
                                    Kamu takut salah membuat memo, surat penting, email atau apapun yang menggukan
                                    bahasa
                                    Inggris? Atau kamu tidak percaya diri untuk menggunakan bahasa Inggris dikalangan
                                    sesama
                                    profesional? Kami siap membantu kamu untuk memperlancar bahasa Inggris mu untuk
                                    keperluan
                                    pekerjaan ataupun bisnis. Materi yang akan diberikan oleh private tutor akan
                                    disesuaikan
                                    dengan kebutuhan dan level kemampuan bahasa Inggrismu. Jangan lupa segera hubungi
                                    kami
                                    untuk
                                    info lebih lanjut mengenai program ini
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="item-boxes services-item wow fadeInDown" data-wow-delay="0.2s">
                            <img src="{{asset('img/programs/ekids.jpg')}}"
                                 style="width: 100%;margin:0 !important;"/>
                            <div class="p-3" style="min-height: 315px;">
                                <h3>English For Kids</h3>
                                <p>
                                    Program English for Kids adalah program baru kami yang dikhususkan untuk anak-anak
                                    usia
                                    taman kanak-kanak sampai sekolah dasar. Kami memberikan metode belajar secara
                                    interaktif
                                    dengan media-media yang sesuai dengan level anak-anak. Dengan konsep in-home
                                    tutoring,
                                    orang
                                    tua bisa mengawasi kondisi dan perkembangan anak-anak mereka tanpa merasa khawatir
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-justify" style="padding:30px;">
                    <h5 style="font-size: 20px; margin: 20px 0;">Program Kelas Daring (Online Class)</h5>
                    <div class="row" style="margin-bottom: 30px !important;">
                        <div class="col-md-6" style="margin-top: 20px !important;"><img
                                src="{{asset('img/online1.jpg')}}"
                                style="max-width: 100%;"
                                alt="info"/></div>
                        <div class="col-md-6" style="margin-top: 20px !important;"><img
                                src="{{asset('img/online2.jpg')}}"
                                style="max-width: 100%;"
                                alt="info"/></div>
                    </div>
                    <p>
                        Kelas IELTS dan General English (Conversation) online di English Supreme bersifat interaktif
                        dengan sistem kelas privat sehingga kamu bisa fokus belajar dan melatih kemampuan bahasa Inggris
                        kamu. Siapa saja yang cocok belajar bahasa Inggris online dengan English Supreme?
                        <br>
                    <ol>
                        <li>
                            Anak sekolahan yang masih bingung dan tidak paham apa yang diajari oleh guru bahasa
                            Inggrisnya di sekolah
                        </li>
                        <li>
                            Mahasiswa yang ingin bisa bahasa Inggris untuk persiapan di dunia kerja
                        </li>
                        <li>
                            Pekerja/karyawan yang dituntut untuk bisa berbahasa Inggris dalam pekerjaannya dan untuk
                            menunjang karirnya
                        </li>
                    </ol>
                    <br>Kelebihan belajar online di English Supreme:<br>
                    <ol>
                        <li>Belajar Online dimana saja</li>
                        <li>Program belajar bahasa Inggris yang jelas <i>step by step</i> nya</li>
                        <li>Praktek 70%, teori 30%</li>
                    </ol>
                    <br>Hubungi kami untuk info lebih lanjut.</p>
                </div>
                <div class="text-justify" style="padding:30px;">
                    <h5 style="font-size: 20px; margin: 20px 0;">Translation Sevice (Jasa Terjemahan)</h5>
                    <p>
                        Selain pengajaran bahasa Inggris, kami juga menyediakan jasa terjemahan paper, buku, dokumen,
                        non sertifikat dengan harga terjangkau.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- Services Section End -->

    <!-- Call to Action Start -->
    {{--<section class="call-action section p-5">--}}
    {{--<div class="container">--}}
    {{--<div class="row justify-content-center">--}}
    {{--<div class="col-10">--}}
    {{--<div class="cta-trial text-center">--}}
    {{--<h3>Are You Ready To Get Started?</h3>--}}
    {{--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod<br>--}}
    {{--Lorem--}}
    {{--ipsum dolor sit amet, consectetuer</p>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</section>--}}
    <!-- Call to Action End -->

    <!-- Features Section Start -->
    {{--<section id="features" class="section">--}}
    {{--<div class="container">--}}
    {{--<div class="section-header">--}}
    {{--<h2 class="section-title">Mengapa Memilih Kami</h2>--}}
    {{--<span>Why</span>--}}
    {{--<p class="section-subtitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos--}}
    {{--debitis.</p>--}}
    {{--</div>--}}
    {{--<div class="row">--}}
    {{--<!-- Start featured -->--}}
    {{--<div class="col-lg-4 col-md-6 col-xs-12">--}}
    {{--<div class="featured-box">--}}
    {{--<div class="featured-icon">--}}
    {{--<i class="lni-layout"></i>--}}
    {{--</div>--}}
    {{--<div class="featured-content">--}}
    {{--<div class="icon-o"><i class="lni-layout"></i></div>--}}
    {{--<h4>Refreshing Design</h4>--}}
    {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor--}}
    {{--incididunt ut--}}
    {{--labore et magna aliqua.</p>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<!-- End featured -->--}}
    {{--<!-- Start featured -->--}}
    {{--<div class="col-lg-4 col-md-6 col-xs-12">--}}
    {{--<div class="featured-box">--}}
    {{--<div class="featured-icon">--}}
    {{--<i class="lni-tab"></i>--}}
    {{--</div>--}}
    {{--<div class="featured-content">--}}
    {{--<div class="icon-o"><i class="lni-tab"></i></div>--}}
    {{--<h4>Fully Responsive</h4>--}}
    {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor--}}
    {{--incididunt ut--}}
    {{--labore et magna aliqua.</p>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<!-- End featured -->--}}
    {{--<!-- Start featured -->--}}
    {{--<div class="col-lg-4 col-md-6 col-xs-12">--}}
    {{--<div class="featured-box">--}}
    {{--<div class="featured-icon">--}}
    {{--<i class="lni-rocket"></i>--}}
    {{--</div>--}}
    {{--<div class="featured-content">--}}
    {{--<div class="icon-o"><i class="lni-rocket"></i></div>--}}
    {{--<h4>Fast & Smooth</h4>--}}
    {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor--}}
    {{--incididunt ut--}}
    {{--labore et magna aliqua.</p>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<!-- End featured -->--}}
    {{--<!-- Start featured -->--}}
    {{--<div class="col-lg-4 col-md-6 col-xs-12">--}}
    {{--<div class="featured-box">--}}
    {{--<div class="featured-icon">--}}
    {{--<i class="lni-database"></i>--}}
    {{--</div>--}}
    {{--<div class="featured-content">--}}
    {{--<div class="icon-o"><i class="lni-database"></i></div>--}}
    {{--<h4>SEO Optimized</h4>--}}
    {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor--}}
    {{--incididunt ut--}}
    {{--labore et magna aliqua.</p>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<!-- End featured -->--}}
    {{--<!-- Start featured -->--}}
    {{--<div class="col-lg-4 col-md-6 col-xs-12">--}}
    {{--<div class="featured-box">--}}
    {{--<div class="featured-icon">--}}
    {{--<i class="lni-leaf"></i>--}}
    {{--</div>--}}
    {{--<div class="featured-content">--}}
    {{--<div class="icon-o"><i class="lni-leaf"></i></div>--}}
    {{--<h4>Clean Code</h4>--}}
    {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor--}}
    {{--incididunt ut--}}
    {{--labore et magna aliqua.</p>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<!-- End featured -->--}}
    {{--<!-- Start featured -->--}}
    {{--<div class="col-lg-4 col-md-6 col-xs-12">--}}
    {{--<div class="featured-box">--}}
    {{--<div class="featured-icon">--}}
    {{--<i class="lni-pencil"></i>--}}
    {{--</div>--}}
    {{--<div class="featured-content">--}}
    {{--<div class="icon-o"><i class="lni-pencil"></i></div>--}}
    {{--<h4>Free 24/7 Support</h4>--}}
    {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor--}}
    {{--incididunt ut--}}
    {{--labore et magna aliqua.</p>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<!-- End featured -->--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</section>--}}
    <!-- Features Section End -->

    <!-- Call To Action Section Start -->
    <section id="cta" class="section" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="cta-text">
                        <h5>Silahkan hubungi kami untuk mengetahui detail lebih lanjut!</h5>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-12 text-right">
                    <a href="https://api.whatsapp.com/send?phone=6287770895888&text=Hallo+dengan+English+Supreme+disini.+Apa+yang+bisa+kami+bantu+sobat%3F"
                       class="btn btn-border">Hubungi Whatsapp</a>
                </div>
            </div>
        </div>
    </section>
    <!-- Call To Action Section Start -->

    <!-- Footer Section Start -->
    <footer id="contact">
        <!-- Footer Area Start -->
        <section class="footer-Content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
                        <h3>English Supreme</h3>
                        <div class="textwidget">
                            <p>
                                english-supreme.com
                            </p>
                        </div>
                        <ul class="footer-social">
                            {{--<li><a class="facebook" href="https://www.facebook.com/english2supreme/"><i--}}
                            {{--class="lni-facebook-filled"></i></a></li>--}}
                            <li><a class="twitter" href="https://twitter.com/english_on_es"><i
                                        class="lni-twitter-filled"></i></a></li>
                            <li><a class="google-plus" href="https://www.instagram.com/english_on_es/"><i
                                        class="lni-instagram"></i></a></li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
                        <div class="widget">
                            {{--<h3 class="block-title">Instagram</h3>--}}
                            {{--<ul class="instagram-footer">--}}
                            {{--<li><a href="#"><img src="img/instagram/insta1.jpg" alt=""></a></li>--}}
                            {{--<li><a href="#"><img src="img/instagram/insta2.jpg" alt=""></a></li>--}}
                            {{--<li><a href="#"><img src="img/instagram/insta3.jpg" alt=""></a></li>--}}
                            {{--<li><a href="#"><img src="img/instagram/insta4.jpg" alt=""></a></li>--}}
                            {{--<li><a href="#"><img src="img/instagram/insta5.jpg" alt=""></a></li>--}}
                            {{--<li><a href="#"><img src="img/instagram/insta6.jpg" alt=""></a></li>--}}
                            {{--</ul>--}}
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
                        <div class="widget">
                            <h3 class="block-title">Website Links</h3>
                            <ul class="menu">
                                <li><a href="#slider-area">Beranda</a></li>
                                <li><a href="#about">English Supreme</a></li>
                                <li><a href="#services">Program dan Jasa Kami</a></li>
                                {{--<li><a href="#features">Why Choose Us</a></li>--}}
                                <li><a href="#contact">Hubungi Kami</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
                        <div class="widget">
                            <h3 class="block-title">Hubungi Kami</h3>
                            <ul class="contact-footer">
                                {{--<li>--}}
                                {{--<strong>Alamat :</strong>--}}
                                {{--<span>1900 Pico Blvd, New York br Centernial, colorado</span>--}}
                                {{--</li>--}}
                                <li>
                                    <strong>Telepon :</strong> <span>+62 877-7089-5888</span>
                                </li>
                                <li>
                                    <strong>Whatsapp :</strong> <span>+62 877-7089-5888</span>
                                </li>
                                <li>
                                    <strong>E-mail :</strong> <span><a href="mailto:englishsupreme.ind@gmail.com">englishsupreme.ind@gmail.com</a></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Footer area End -->

        <!-- Copyright Start  -->
        <div id="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="site-info float-left">

                        </div>
                        <div class="float-right">
                            <p>&copy; Copyright 2020, English Supreme</p>
                            {{--<ul class="nav nav-inline">--}}
                            {{--<li class="nav-item">--}}
                            {{--<a class="nav-link active" href="#">About Prime</a>--}}
                            {{--</li>--}}
                            {{--<li class="nav-item">--}}
                            {{--<a class="nav-link" href="#">TOS</a>--}}
                            {{--</li>--}}
                            {{--<li class="nav-item">--}}
                            {{--<a class="nav-link" href="#">Return Policy</a>--}}
                            {{--</li>--}}
                            {{--<li class="nav-item">--}}
                            {{--<a class="nav-link" href="#">FAQ</a>--}}
                            {{--</li>--}}
                            {{--<li class="nav-item">--}}
                            {{--<a class="nav-link" href="#">Contact</a>--}}
                            {{--</li>--}}
                            {{--</ul>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Copyright End -->

    </footer>
    <!-- Footer Section End -->
@endsection
